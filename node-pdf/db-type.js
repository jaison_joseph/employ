// @flow

// prettier-ignore
type DatabaseType = {
  /////////////
  // OBJECTS //
  /////////////

  'organization': {
    +name: string,
    +type: OrganizationTypeType,
  },

  'role': {
    +name: string,
  },

  'shift': {
    +endDate?: Date,
    +roleId: RoleIdType,
    +startDate: Date,
  },

  'shiftAssignment': {
    +endDate?: Date,
    +shiftId: ShiftIdType,
    +startDate: Date,
    +userId?: UserIdType,
  },

  'shiftSlot': {
    +date: Date,
    +isDeletion: boolean,
    +replacementShiftAssignmentId?: ShiftAssignmentIdType,
  },

  'shiftTiming': {
    +endDate?: Date,
    +endTime: TimeDataType,
    +repeatingDays?: TimeDayType[],
    +shiftId: ShiftIdType,
    +startDate: Date,
    +startTime: TimeDataType,
  },

  'team': {
    +hiringManagerUserId: UserIdType,
    +hiringVideoUrl?: string,
    +onboardDate?: Date,
    +location: string,
    +openJobIds?: JobIdType[],
    +organizationId: OrganizationIdType,
    +schedulingManagerUserId: UserIdType,
  },

  'user': {
    +appBadgeCount?: number,
    +birthday?: string,
    +email: string,
    +education?: string,
    +experiences?: UserExperienceType[],
    +facebookId: string,
    +firstName: string,
    +gender?: UserGenderType,
    +isEmployUser?: boolean,
    +isTestUser?: boolean,
    +languageIds: LanguageIdType[],
    +lastName: string,
    +phone?: string,
    +phoneVerifiedDate?: Date,
    +photoUrl?: string,
    +photoCropData?: PhotoCropDataType,
    +workerDistanceThreshold?: number,
    +workerIndustryIds?: IndustryIdType[],
    +workerOnboardDate?: Date,
    +workerVideoUrl?: string,
  },

  //////////
  // SETS //
  //////////

  'shift-shiftAssignments': {
    [ShiftIdType]: {
      [ShiftAssignmentIdType]: {
        +startDate: Date,
        +endDate?: Date,
      },
    },
  },

  'shift-shiftTimings': {
    [ShiftIdType]: {
      [ShiftTimingIdType]: {
        +startDate: Date,
        +endDate?: Date,
      },
    },
  },

  'shiftAssignment-shiftSlots': {
    [ShiftAssignmentIdType]: {
      [ShiftSlotIdType]: Object,
    },
  },

  'shiftTiming-shiftSlots': {
    [ShiftTimingIdType]: {
      [ShiftSlotIdType]: Object,
    },
  },

  'team-roles': {
    [TeamIdType]: {
      [RoleIdType]: Object,
    },
  },

  'team-shifts': {
    [TeamIdType]: {
      [ShiftIdType]: {
        +startDate: Date,
        +endDate?: Date,
      },
    },
  },

  'team-users': {
    [TeamIdType]: {
      [UserIdType]: Object,
    },
  },

  'user-teams': {
    [UserIdType]: {
      [TeamIdType]: Object,
    },
  },
}
