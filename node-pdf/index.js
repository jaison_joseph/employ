/** 
 * Setup required libraries
 * 1. fs
 * 2. ejs
 * 3. pdf
 * 4. config
 * 5. nodemailer
 * 6. moment
 */
const fs = require('fs')
const ejs = require('ejs');
const pdf = require('html-pdf');
const config = require('config');
const nodemailer = require('nodemailer');
const moment = require('moment');

/**
 * If startOfWeek and endOfWeek not specified then get the current week's start and end dates 
 * TODO: Inorder to test with current weeks data in json, use the commented code or you can manually input a startdate and enddate
 */
let startOfWeek = moment('2018-03-11T01:33:05.183Z').startOf('week').toDate();  // Start date of week input- moment().startOf('week').toDate() 
let endOfWeek = moment('2018-03-17T01:33:05.183Z').endOf('week').toDate(); // End date of week input- moment().endOf('week').toDate()
let teamId = "-L4iEC-swKjUeOZzMWxE";// TeamId input.

/**
 * Options used in PDF Generation 
 */
let options = { format: 'Legal', quality: 75, orientation: "portrait" };
/**
 * Nodemailer transporter
 */
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.get("mailConfig.smtpUsername"),
        pass: config.get("mailConfig.smtpPassword")
    }
})


/**
 * Read JSON data in db-export.json file 
 * Author  : iLeaf Solutions 
 */
fs.readFile('db-export.json', 'utf8', function (err, jsonData) {
    if (err) {
        console.log(err);
        return false;
    } else {

        /**
          * Add current week startDate and endDate 
          */
        jsonOutput = {
            "startDate": moment(startOfWeek).format('D MMMM'),
            "endDate": moment(endOfWeek).format('D MMMM'),
            "days": [],
            "companyLogo": config.get('imageUrl') + 'logo.png',
            "weekData": config.get('imageUrl') + 'week-text.png',
            "emptyImageUrl": config.get('imageUrl') + 'empty-avatar.png'
        };


        let jsonObject = JSON.parse(jsonData);
        let teamShifts = jsonObject["team-shifts"];


        /**
         * Get teamShifts from jsonObject uing teamId
         */
        let shifts = teamShifts[teamId];


        /**
         * Looping shifts
         */
        for (let shiftId in shifts) {
            if (shifts.hasOwnProperty(shiftId)) {

                /**
                 * Consider only data have startDate and endDate.
                 */
                if (shifts[shiftId].startDate && shifts[shiftId].endDate) {

                    let startDate = moment(shifts[shiftId].startDate);
                    let endDate = moment(shifts[shiftId].endDate);


                    /**
                     * if startOfWeek and endOfWeek week data only 
                     */
                    if (moment(startOfWeek).diff(startDate) <= 0 && moment(endOfWeek).diff(endDate) >= 0) {


                    /**
                     * Get shift-ShiftTimings form jsonObject uing shiftId
                     */
                    let shiftShiftTimings = jsonObject["shift-shiftTimings"][shiftId];

                    if (shiftShiftTimings) {

                        /**
                         * find shiftTime Id and get shiftTiming data
                         */
                        for (let shiftTimingId in shiftShiftTimings) {
                            if (shiftShiftTimings.hasOwnProperty(shiftTimingId)) {
                                let shiftTiming = jsonObject["shiftTiming"][shiftTimingId];

                                if (shiftTiming) {

                                    /**
                                     * Get shift-shiftAssignments form jsonObject using shiftTimingId
                                     */
                                    let shiftShiftAssignments = jsonObject["shift-shiftAssignments"][shiftId];

                                    if (shiftShiftAssignments) {


                                        /**
                                         * find shiftAssignment Id and get shiftTiming data
                                         */
                                        for (let shiftAssignmentId in shiftShiftAssignments) {
                                            if (shiftShiftAssignments.hasOwnProperty(shiftAssignmentId)) {
                                                let shiftAssignment = jsonObject["shiftAssignment"][shiftAssignmentId];
                                                let userId = shiftAssignment.userId;
                                                if (userId) {

                                                    /**
                                                     * Get user form jsonObject using userId
                                                     */
                                                    let user = jsonObject["user"][userId];

                                                    if (user) {

                                                        /**
                                                         * Get shiftAssignment-shiftSlots form jsonObject using shiftAssignmentId
                                                         */
                                                        let shiftAssignmentShiftSlots = jsonObject["shiftAssignment-shiftSlots"][shiftAssignmentId];

                                                        /**
                                                         * If any shiftAssignmentId available in shiftAssignment-shiftSlots, exclude that user from the list.
                                                         */
                                                        if (shiftAssignmentShiftSlots) {//exclude that user from the list
                                                            console.log("Skip user, user Id exist shiftAssignmentShiftSlots")
                                                        } else {

                                                            let assignmentShiftId = shiftAssignment.shiftId;

                                                            if (assignmentShiftId) {
                                                                /**
                                                                * Get shift form jsonObject using ShiftId
                                                                */
                                                                let shift = jsonObject["shift"][assignmentShiftId];

                                                                if (shift) {
                                                                    let roleId = shift.roleId;

                                                                    if (roleId) {
                                                                        /**
                                                                         * Get role form jsonObject using roleId
                                                                         */
                                                                        let role = jsonObject["role"][roleId];

                                                                        if (role) {
                                                                            let roleName = role.name;

                                                                            //#################################|
                                                                            //#   Create Out Put JSON Object  #|
                                                                            //#################################|

                                                                            /**
                                                                             * Update or upload Days in jsonOutput
                                                                             */
                                                                            let dateIndex = jsonOutput.days.findIndex(findDateIndex);

                                                                            if (dateIndex < 0) { //date not exist 
                                                                                jsonOutput.days.unshift({
                                                                                    startDay: moment(startDate).format('dddd'),
                                                                                    date: moment(startDate).format('D MMMM'),
                                                                                    shifts: []
                                                                                });
                                                                                dateIndex = 0;
                                                                            }
                                                                            /**
                                                                             * Find Index of date
                                                                             * @param {Array} day 
                                                                             */
                                                                            function findDateIndex(day) {
                                                                                return day.date == moment(startDate).format('D MMMM');
                                                                            }


                                                                            /**
                                                                             * Update or upload shifts in jsonOutput
                                                                             */
                                                                            let shiftIndex = jsonOutput.days[dateIndex].shifts.findIndex(findShiftIndex);

                                                                            if (shiftIndex < 0) { //shift not exist 
                                                                                jsonOutput.days[dateIndex].shifts.unshift({
                                                                                    position: roleName,
                                                                                    timeArray: []
                                                                                });
                                                                                shiftIndex = 0;
                                                                            }
                                                                            /**
                                                                             * Find Index of shift
                                                                             * @param {Array} shift 
                                                                             */
                                                                            function findShiftIndex(shift) {
                                                                                return shift.position == roleName;
                                                                            }


                                                                            /**
                                                                             * Update or upload times in jsonOutput
                                                                             */
                                                                            timeIndex = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.findIndex(findTimeIndex);

                                                                            if (timeIndex < 0) { //time not exist 
                                                                                jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.unshift({
                                                                                    startTime: moment(shiftTiming.startTime).format('HH.mm a'),
                                                                                    endTime: moment(shiftTiming.endTime).format('HH.mm a'),
                                                                                    users: []
                                                                                });
                                                                                timeIndex = 0;
                                                                            }
                                                                            /**
                                                                             * Find Index of time
                                                                             * @param {Array} time 
                                                                             */
                                                                            function findTimeIndex(time) {
                                                                                return (time.startTime == moment(shiftTiming.startTime).format('HH.mm a') && time.endTime == moment(shiftTiming.endTime).format('HH.mm a'));
                                                                            }


                                                                            /**
                                                                             * Update or upload user in jsonOutput
                                                                             */
                                                                            userIndex = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.findIndex(findUserIndex);

                                                                            if (userIndex < 0) { //user not exist 
                                                                                jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.unshift({
                                                                                    id: userId,
                                                                                    name: user.firstName,
                                                                                    imageUrl: user.photoUrl ? user.photoUrl : ""
                                                                                });
                                                                                userIndex = 0;
                                                                            }
                                                                            /**
                                                                             * Find Index of user
                                                                             * @param {Array} user 
                                                                             */
                                                                            function findUserIndex(user) {
                                                                                return user.id == user.userId;
                                                                            }

                                                                            //#################################|
                                                                            //#        End JSON Object        #|
                                                                            //#################################|

                                                                        }
                                                                    }
                                                                }

                                                            } else {
                                                                console.log("Assignment Shift Id Not Found in input json object, shiftId: ", shiftId)
                                                            }
                                                        }
                                                    } else {
                                                        console.log("user Not Found in input json object and userId is :", userId);
                                                    }
                                                } else {
                                                    console.log("User Id Not Found in input json object, hiftId: ", shiftId)
                                                }// End if User Id
                                            }
                                        }//End Loop shiftShiftAssignments
                                    } else {
                                        console.log("shiftShiftAssignments Not Found in input json object and shiftId is :", shiftId)
                                    }
                                } else {
                                    console.log("shiftTiming Not Found in input json object and shiftTimingId is :", shiftTimingId)
                                }
                            }
                        }// End Loop shiftShiftTimings
                    } else {
                        console.log("shiftShiftTimings Not Found in input json object and shiftId is :", shiftId)
                    }
                    } else {
                        console.log("Skip data, Not this Week data", shiftId)
                    }

                } else { //skipped shiftId
                    console.log("skip shiftId, Empty startDate or endDate", shiftId)
                }
            }
        }//End shifts for loop


        /**
         * Sort date
         */
        jsonOutput.days.sort(function (day1, day2) {
            return moment.utc(new Date(day1.date)).diff(moment.utc(new Date(day2.date)))
        });

        for (let day in jsonOutput.days) {
            
            jsonOutput.days[day].shifts.sort(compareByName);

            for (let shift in jsonOutput.days[day].shifts) {
                /**
                 * Sort start Time
                 */
                jsonOutput.days[day].shifts[shift].timeArray.sort(function (time1, time2) {
                    return moment.utc(time1.startTime).diff(moment.utc(time2.startTime))
                });

                for (let time in jsonOutput.days[day].shifts[shift].timeArray) {

                    /**
                     * Sort User Name
                     */
                    for (let user in jsonOutput.days[day].shifts[shift].timeArray[time]) {


                        jsonOutput.days[day].shifts[shift].timeArray[time].users.sort(compareByName);
                    }
                }
            }
        }


        /**
         * Name Sorting 
         * @param {*} user1 
         * @param {*} user2 
         */
        function compareByName(user1, user2) {
            if (user1.name < user2.name)
                return -1;
            if (user1.name > user2.name)
                return 1;
            return 0;
        }

        createPdfFile(jsonOutput);

    }//End readFile error else 

});//End readFile 



/**
 * PDF Generation 
 * Author : iLeaf Solutions 
 * @param {Object} htmlInput 
 */
function createPdfFile(jsonData) {
    let htmlInput = {
        data: jsonData
    };
    ejs.renderFile('./pdf-template.ejs', htmlInput, function (err, renderHtml) {
        if (err)
            console.log("Error from ejs rendering: ", err);
        else {
            pdf.create(renderHtml, options).toFile('./wall.pdf', function (err, res) {
                if (err)
                    console.log("Error from pdf generation: ", err);
                else {
                    console.log(res); // { filename: '/app/businesscard.pdf' }

                    let mailOptions = {
                        from: '"' + config.get("mailConfig.fromName") + '" <' + config.get("mailConfig.fromMail") + '>',// sender address
                        to: config.get("mailConfig.toAddress"), // list of receivers 
                        subject: "Shift Details",
                        html: "Hello,<br><br>Please find the attached pdf.<br>",
                        attachments: [
                            {   // file on disk as an attachment
                                filename: 'wall.pdf',
                                path: config.get('pdfPath') + 'wall.pdf' // stream this file
                            }
                        ]
                    };

                    // Send the Mail
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            console.log("Error from mail sending: ", error);
                        } else {
                            console.log("Mail sent successfully")
                        }
                    });
                }
            });
        }
    });
}
