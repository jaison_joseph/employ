# iLeaf

## Setup

#### Code

Use the latest ES2017 features; including arrow functions, `async`/`await`, spread/rest, etc.

Use [Flow](https://flow.org/) to add static typing to the code. React Native already comes with this set up; just make sure to utilize it.

Use [Prettier](https://prettier.io/) to ensure proper code formatting. Use the following `.prettierrc` config:

```json
{
  "arrowParens": "avoid",
  "printWidth": 80,
  "parser": "flow",
  "semi": false,
  "singleQuote": true,
  "trailingComma": "es5",
}
```

#### Folder structure

Use the following structure to organize the modules:

```
employ/
├─ actions/      # All Redux actions that update the store
├─ components/   # All React components (and containers)
├─ constants/    # All constants for styling, action types, or anything else
├─ reducers/     # All Redux state action reducers
├─ selectors/    # All Redux state selectors
├─ utils/        # All utility functions used in actions, components, reducers, & selectors
└─ index.js
```

#### Redux

The state must have a top-level `jaison` domain which should contain the entire state of the project:

```js
const state = {
  jaison: {
    // All state goes here
  },
}
```

Make sure all the reducers and selectors are built accordingly.

Utilize `redux-thunk` to handle async actions.

<br />

---

<br />

## Current project: Profile settings

Develop a new React Native project with a view that loads a user from the database and then allows the user to update the following properties:

  * `firstName`
  * `lastName`
  * `photoUrl`

#### Firebase

The database shape looks like this:

```jsx
{
  // ...other properties
  'user': {
    [$userId]: UserObject,
  },
}
```

And the `UserObject` looks like this:

```jsx
type UserObject = {
  firstName: string,
  lastName: string,
  photoUrl: string,
}
```
