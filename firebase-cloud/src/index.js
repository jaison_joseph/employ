const functions = require('firebase-functions');
const admin = require('firebase-admin');
const lme = require('lme');
const Promise = require('bluebird');

function objValues(obj) {
	let values = Object.keys(obj).map(function(key) {
		return obj[key];
	});

	return values;
}

admin.initializeApp(functions.config().firebase);

exports.saveOrganization = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const org = await admin
			.database()
			.ref('/organization')
			.push(req.body);
		return res.send({ org_key: org.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveTeam = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const team = await admin
			.database()
			.ref('/team')
			.push(req.body);
		return res.send({ team_key: team.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveJob = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const job = await admin
			.database()
			.ref('/job')
			.push(req.body);

		return res.send({ job_key: job.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveUser = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const user = await admin
			.database()
			.ref('/user')
			.push(req.body);

		return res.send({ user_key: user.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveUserLikedJobs = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const job = await admin
			.database()
			.ref('/userLikedJobs')
			.push(req.body);

		return res.send({ liked_job_key: job.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveUserPassedJobs = functions.https.onRequest(async (req, res) => {
	delete req.body.collection;

	try {
		const job = await admin
			.database()
			.ref('/userPassedJobs')
			.push(req.body);

		return res.send({ passed_job_key: job.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveUserTeams = functions.https.onRequest(async (req, res) => {
	try {
		const team = await admin
			.database()
			.ref('/userTeams')
			.push(req.body);

		return res.send({ user_team_key: team.key });
	} catch (err) {
		return res.send(err);
	}
});

exports.saveTypeIndustry = functions.https.onRequest(async (req, res) => {
	try {
		const industry = await admin
			.database()
			.ref('/typeIndustry')
			.push(req.body);
		return res.status(200).send({ industry_key: industry.key });
	} catch (err) {
		return res.send(500).send(err);
	}
});

/**
 * @api {post} https://us-central1-example-b8f70.cloudfunctions.net/filterTeams filterTeams
 * @apiName filterTeams
 *
 * @apiParam {String[]} industryIds industryIds.
 * @apiParam {String} userId userId.
 * @apiParam {String} count count.
 * @apiParam {String} [startingTeamId] startingTeamId.
 *
 * @apiSuccess {String} status status.
 * @apiSuccess {Object} teams filtered teams.
 */

exports.newFilter = functions.https.onRequest(async (req, res) => {
	let count: number = 10;
	let orgKeys: Array<string> = [];
	let teamIds: Array<string> = [];
	let organizations: Array<string> = [];
	let orgIds: Array<string> = [];
	let teamData: Array<string> = [];
	let jobIds: Array<string> = [];
	let userTeamIds: Array<string> = [];
	let teamKeyIds: Array<string> = [];
	let usersTeamIds: Array<string> = [];
	let data: Array<string> = [];

	const industries: Array<string> = req.body.industryIds;

	if (!industries || industries.length === 0 || !req.body.userId) {
		return res.status(400).send({
			status: 'Failed',
			msg: 'Invalid Parameters!!'
		});
	}

	const userId: string = req.body.userId;

	const startingTeamId = req.body.teamId ? req.body.teamId : '';

	if (req.body.count) {
		count = parseInt(req.body.count);
	}

	try {
		// Finding organizations in given industries from collection - typeIndustry.
		orgKeys = await Promise.map(
			industries,
			key_str =>
				new Promise((resolve, reject) =>
					admin
						.database()
						.ref('/typeIndustry')
						.orderByChild('industry')
						.equalTo(key_str)
						.once('value', snapshot => {
							return resolve(snapshot.val());
						})
				)
		);

		// Mapping over given value for getting organization type name.
		organizations = objValues(orgKeys[0]).map(function(industry) {
			return industry.org;
		});

		// If no organizations found returning an error.
		if (organizations.length === 0) {
			return res.status(500).send({
				status: 'Failed',
				msg: 'Matching organization not found'
			});
		}

		// Finding organization collection ids for filtering teams.
		await Promise.map(
			organizations,
			key_str =>
				new Promise((resolve, reject) =>
					admin
						.database()
						.ref('/organization')
						.orderByChild('type')
						.equalTo(key_str)
						.once('value', snapshot => {
							if (snapshot.val()) {
								let t = snapshot.val();
								orgIds.push(Object.keys(t));
								resolve();
							}
							resolve();
						})
				)
		);

		// Removing duplicates from array.
		let organizationIds: Array<string> = [].concat.apply([], orgIds);
		organizationIds = Array(...new Set(organizationIds));

		// Finding user passed job ids.
		await new Promise((resolve, reject) =>
			admin
				.database()
				.ref('/userLikedJobs')
				.orderByChild('userId')
				.equalTo(userId)
				.once('value', snapshot => {
					if (snapshot.val()) {
						let t = snapshot.val();
						let q = t[Object.keys(t)[0]];
						jobIds = jobIds.concat(q.jobId);
						resolve();
					} else {
						resolve();
					}
				})
		);

		// Finding user liked job ids.
		await new Promise((resolve, reject) =>
			admin
				.database()
				.ref('/userPassedJobs')
				.orderByChild('userId')
				.equalTo(userId)
				.once('value', snapshot => {
					if (snapshot.val()) {
						let t = snapshot.val();
						let q = t[Object.keys(t)[0]];
						jobIds = jobIds.concat(q.jobId);
						resolve();
					} else {
						resolve();
					}
				})
		);

		// Removing duplicates.
		jobIds = Array(...new Set(jobIds));

		// Finding user involved team ids.
		await new Promise((resolve, reject) => {
			admin
				.database()
				.ref('/userTeams')
				.orderByChild('userId')
				.equalTo(userId)
				.once('value', snapshot => {
					if (snapshot.val()) {
						let t = snapshot.val();
						let q = t[Object.keys(t)[0]];
						usersTeamIds = usersTeamIds.concat(q.teamId);
						resolve();
					} else {
						resolve();
					}
				});
		});

		// Iterating through teams with organization ids.
		await Promise.map(
			organizationIds,
			key_str =>
				new Promise((resolve, reject) => {
					admin
						.database()
						.ref('/team')
						.orderByChild('organizationId')
						.equalTo(key_str)
						.once('value', snapshot => {
							// If team with given organization id is present pass it for other tests.
							if (snapshot.val()) {
								let t = snapshot.val();
								let tKeys = Object.keys(t);
								if (tKeys.length === 0) {
									return resolve();
								}
								// Iterating through found values.
								// If team id present in user_teams then it will filter out.
								tKeys.forEach(key => {
									let keyData = {};
									if (
										usersTeamIds.length !== 0 &&
										usersTeamIds.indexOf(key) > -1
									) {
										return;
									}
									if (
										!t[key].hiringVideoUrl ||
										t[key].openJobIds.length === 0
									) {
										return;
									}
									keyData[key] = t[key];
									// If users don't have any passed/liked jobs it will filter out.
									if (jobIds.length !== 0) {
										// Checking with the open job ids present in user's passed/liked jobs
										// if present it will filter out.
										let found = t[key].openJobIds.some(r =>
											jobIds.includes(r)
										);
										if (found) {
											return;
										}
										if (!found) {
											teamKeyIds.push(keyData);
										}
									} else {
										teamKeyIds.push(keyData);
									}
								});
								resolve();
							}
							resolve();
						});
				})
		);
		// If result contain 0 team ids returning the response.
		if (teamKeyIds.length === 0) {
			return res.status(200).send({
				data: data
			});
		}
		// Function for sorting the result according to time stamp - Doing just because didn't found
		// any method to filter and sorting in firebase database.
		let keyDateArray = [];
		teamKeyIds.forEach(k => {
			let key = Object.keys(k)[0];
			let values = objValues(k)[0];
			let obj = {
				key: key,
				date: values.createdDate
			};
			keyDateArray.push(obj);
		});
		// Sorting with date.
		keyDateArray.sort(function(a, b) {
			return b.date - a.date;
		});
		// If starting team id is given starting from there else finding the last 'n' keys.
		let keyArray = [];
		if (!startingTeamId) {
			let items = keyDateArray.slice(0, count).map(i => {
				keyArray.push(i.key);
				return;
			});
		} else {
			let isFound: boolean = false;
			keyDateArray.forEach(team => {
				if (team.key === startingTeamId) isFound = true;
				if (isFound && keyArray.length < count) {
					keyArray.push(team.key);
				}
			});
		}

		// Finding the team details of 'n' keys.
		await Promise.map(
			keyArray,
			key_str =>
				new Promise((resolve, reject) =>
					admin
						.database()
						.ref('/team')
						.orderByKey()
						.equalTo(key_str)
						.once('value', snapshot => {
							if (snapshot.val()) {
								data.push(snapshot.val());
								resolve();
							}
							resolve();
						})
				)
		);

		return res.status(200).send({
			data: data
		});
	} catch (err) {
		console.log(err);
		return res.status(500).send({
			status: 'Failed',
			err: err
		});
	}
});
