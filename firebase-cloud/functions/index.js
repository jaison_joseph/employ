const functions = require('firebase-functions');
const admin = require('firebase-admin');
const lme = require('lme');

const typeIndustry = {
	bar: 'food-beverage',
	club: 'food-beverage',
	coffeeShop: 'food-beverage',
	dessertShop: 'food-beverage',
	hotel: 'hospitality',
	lounge: 'food-beverage',
	restaurant: 'food-beverage',
	retailShop: 'retail',
	startup: 'startup'
};

const getTeams = req => {
	let count = 10;
	if (req.body.count) {
		count = parseInt(req.body.count);
	}
	return new Promise((resolve, reject) => {
		if (req.body.startingTeamId) {
			admin
				.database()
				.ref('/team')
				.orderByKey()
				.startAt(req.body.startingTeamId)
				.limitToFirst(count)
				.once('value', snapshot => resolve(snapshot.val()));
		} else {
			admin
				.database()
				.ref('/team')
				.orderByChild('createdDate')
				.limitToLast(count)
				.once('value', snapshot => resolve(snapshot.val()));
		}
	});
};

admin.initializeApp(functions.config().firebase);

/**
 * @api {post} https://us-central1-example-b8f70.cloudfunctions.net/addData Adding collections
 * @apiName Adding collections
 * 
 * Eg.user ={createdDate: new Date().getTime(),languageIds: ['en', 'zh'],workerDistanceThreshold: 4,
	workerIndustryIds: ['food-beverage', 'hospitality'],workerVideoUrl: 'fgjhgjf',name: 'user1',collection: 'users'}
 * Eg.organization = {createdDate: new Date().getTime(),type: 'dessertShop',collection: 'organization'};
 * Eg.job = {createdDate: new Date().getTime(),languageIds: ['en', 'zh'],collection: 'job'};
 * Eg.team = {name: 'team1',createdDate: new Date().getTime(),openJobIds: [],hiringVideoUrl: 'https://www.youtube.com/watch?v=O17OWyx08Cg',
	organizationId: orgId1,collection: 'team'}
 */
exports.addData = functions.https.onRequest((req, res) => {
	let ref;
	switch (req.body.collection) {
		case 'users':
			ref = '/users';
			break;
		case 'organization':
			ref = '/organization';
			break;
		case 'team':
			ref = '/team';
			break;
		case 'job':
			ref = '/job';
			break;
		case 'userTeam':
			ref = '/userTeam';
			break;
		case 'userLikedJobs':
			ref = '/userLikedJobs';
			break;
		case 'userPassedJobs':
			ref = '/userPassedJobs';
			break;
		default:
			break;
	}
	delete req.body.collection;
	admin
		.database()
		.ref(ref)
		.push(req.body)
		.then(snapshot => {
			return res.send(snapshot.key);
		})
		.catch(err => {
			return res.send(err);
		});
});

/**
 * @api {post} https://us-central1-example-b8f70.cloudfunctions.net/filterTeams filterTeams
 * @apiName filterTeams
 *
 * @apiParam {String[]} industryIds industryIds.
 * @apiParam {String} userId userId.
 * @apiParam {String} count count.
 * @apiParam {String} [startingTeamId] startingTeamId.
 *
 * @apiSuccess {String} status status.
 * @apiSuccess {Object} teams filtered teams.
 */
exports.filterTeams = functions.https.onRequest((req, res) => {
	const industries = req.body.industryIds;
	const userId = req.body.userId;
	let isTeam = true;

	let jobIds = [];
	let teams = [];
	let org = [];
	let orgIds = [];
	let newTeams = [];
	let new1Teams = [];
	let finalTeams = [];
	let teamKeys = [];
	let abc = [];

	if (industries.length === 0) {
		return res.status(400).send({
			status: 'Failed',
			msg: 'No Industry Ids'
		});
	}
	industries.forEach(industry => {
		let keys = Object.keys(typeIndustry);
		keys.forEach(k => {
			if (typeIndustry[k] === industry) org.push(k);
		});
	});

	getTeams(req)
		.then(data => {
			if (data.length !== 0) {
				teamKeys = Object.keys(data);
				return Promise.all(
					org.map(
						key_str =>
							new Promise((resolve, reject) =>
								admin
									.database()
									.ref('/organization')
									.orderByChild('type')
									.equalTo(key_str)
									.once('value', snapshot =>
										resolve(snapshot.val())
									)
							)
					)
				);
			}
			isTeam = false;
			return Promise.reject(new Error('Team is null'));
		})
		.then(orgs => {
			if (orgs && orgs.length !== 0) {
				orgs.forEach(org => {
					if (org) {
						let k = Object.keys(org);
						orgIds = orgIds.concat(k);
					}
				});
			}
			return Promise.all(
				teamKeys.map(
					key_str =>
						new Promise((resolve, reject) =>
							admin
								.database()
								.ref('/team')
								.orderByKey()
								.equalTo(key_str)
								.once('value', snapshot => {
									resolve(snapshot.val());
								})
						)
				)
			);
		})
		.then(dataE => {
			if (dataE.length === 0) {
				isTeam = false;
				return Promise.reject(new Error('Team is null'));
			}

			let i = 0;
			dataE.forEach(obj => {
				if (obj) {
					Object.keys(obj).forEach(val => {
						if (
							dataE[i][val].openJobIds &&
							dataE[i][val].openJobIds.length !== 0 &&
							dataE[i][val].hiringVideoUrl
						) {
							if (
								orgIds.indexOf(dataE[i][val].organizationId) >
								-1
							) {
								teams.push(val);
							}
						}
					});
				}
				i++;
			});

			return Promise.resolve();
		})
		.then(() => {
			if (teams && teams.length === 0) {
				isTeam = false;
				return Promise.reject(new Error('Team is null'));
			}
			if (teams && teams.length !== 0) {
				return Promise.all(
					teams.map(
						key_str =>
							new Promise((resolve, reject) =>
								admin
									.database()
									.ref('/userTeam')
									.orderByChild('teamId')
									.equalTo(key_str)
									.once('value', snapshot => {
										if (snapshot.val()) {
											let t = snapshot.val();
											let q = t[Object.keys(t)[0]];
											if (q.userId === userId)
												newTeams.push(key_str);
											resolve(snapshot.val());
										} else {
											resolve();
										}
									})
							)
					)
				);
			}
			return Promise.resolve();
		})
		.then(() => {
			if (newTeams.length === 0 && teams.length === 0) {
				return Promise.reject(new Error('Team is null'));
			}
			return new Promise((resolve, reject) => {
				function arr_diff(a1, a2) {
					var a = [],
						diff = [];

					for (let j = 0; j < a1.length; j++) {
						a[a1[j]] = true;
					}

					for (var k = 0; k < a2.length; k++) {
						if (a[a2[k]]) {
							delete a[a2[k]];
						} else {
							a[a2[k]] = true;
						}
					}

					for (var p in a) {
						diff.push(p);
					}

					return diff;
				}
				new1Teams = arr_diff(newTeams, teams);
				resolve();
			});
		})
		.then(() => {
			return new Promise((resolve, reject) =>
				admin
					.database()
					.ref('/userLikedJobs')
					.orderByChild('userId')
					.equalTo(userId)
					.once('value', snapshot => {
						if (snapshot.val()) {
							let t = snapshot.val();
							let q = t[Object.keys(t)[0]];
							jobIds.push(q.jobId);
							resolve();
						} else {
							resolve();
						}
					})
			);
		})
		.then(() => {
			return new Promise((resolve, reject) =>
				admin
					.database()
					.ref('/userPassedJobs')
					.orderByChild('userId')
					.equalTo(userId)
					.once('value', snapshot => {
						if (snapshot.val()) {
							let t = snapshot.val();
							let q = t[Object.keys(t)[0]];
							jobIds.push(q.jobId);
							resolve();
						} else {
							resolve();
						}
					})
			);
		})
		.then(() => {
			jobIds = Array(...new Set(jobIds));
			if (new1Teams.length === 0) {
				isTeam = false;
				return Promise.reject(new Error('Team is null'));
			}
			return Promise.all(
				new1Teams.map(
					key_str =>
						new Promise((resolve, reject) =>
							admin
								.database()
								.ref('/team')
								.orderByKey()
								.equalTo(key_str)
								.once('value', snapshot => {
									let t = snapshot.val();
									let q = t[Object.keys(t)[0]];
									let found = false;
									if (jobIds.length !== 0) {
										found = q.openJobIds.some(r =>
											jobIds.includes(r)
										);
									}
									if (!found) {
										finalTeams.push(snapshot.val());
									}
									resolve();
								})
						)
				)
			);
		})
		.then(data => {
			return res.status(200).send({
				status: 'Success',
				teams: finalTeams
			});
		})
		.catch(err => {
			if (!isTeam) {
				return res.status(500).send({
					status: 'Failed',
					msg: err
				});
			}
			return res.status(500).send({
				status: 'Failed',
				msg: err
			});
		});
});
