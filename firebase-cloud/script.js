var got = require("got");
var program = require('commander');

program
.version('0.1.0')
.option('-c, --count [type]', 'Count', 10)
.option('-o, --object [type]', 'Object type', "team")
.parse(process.argv);


if(program.count && program.object)
	console.log('Creating %s %s ...', program.count, program.object);


function randomIndustry(){
	let ind_list = ['food_beverage', 'hospitality', 'retail', 'startup'];
	return ind_list[Math.floor(Math.random()*ind_list.length)];
}


function randomOrganization(){
	let org_type_list = [
	'bar', 
	'club', 
	'coffee_shop', 
	'dessert_shop', 
	'hotel', 
	'lounge', 
	'restaurant', 
	'retail_shop', 
	'startup'
	];

	return org_type_list[Math.floor(Math.random()*org_type_list.length)];
}


function randomUrl(){
	let text = "";
	let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return `http://${text}.com/${text}`;
}


function makeOrganization(){
	let organization_data = {
		createdDate: + new Date(),
		type: randomOrganization()
	};

	return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveOrganization', { body: organization_data, json: true });
}


function makeJob(){
	let job_data = {
		createdDate: + new Date(),
		languageIds: ["en", "zh"]
	};

	return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveJob', { body: job_data, json: true });
}


function makeTeam() {
	let team_data = {
		createdDate: + new Date(),
		hiringVideoUrl: randomUrl(),
		openJobIds: []
	};

	return makeOrganization().then(org_res => {
		team_data.organizationId = org_res.body.org_key;
		return makeJob();
	})
	.then(job_res => {
		team_data.openJobIds.push(job_res.body.job_key);
		return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveTeam', { body: team_data, json: true });
	})
}

function makeATeam(){
	makeTeam()
	.then(team_res =>{
		console.log(team_res.body.team_key);
	})
	.catch(err => {
		console.log('err', err);
	});
}


function makeUserLikedJobs(userId, jobId){
	let jobIds = [];
	jobIds.push(jobId);

	let user_liked_job_data = {
		userId: userId,
		jobId: jobIds
	};

	return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveUserLikedJobs', { body: user_liked_job_data, json: true });
}


function makeUserPassedJobs(userId, jobId){
	let jobIds = [];
	jobIds.push(jobId);

	let user_passed_job_data = {
		userId: userId,
		jobId: jobIds
	};

	return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveUserPassedJobs', { body: user_passed_job_data, json: true });	
}


function makeUser(){
	let user_data = {
		createdDate: + new Date(),
		languageIds: ["en", "zh"],
		workerDistanceThreshold: 10,
		workerIndustryIds: [],
		workerVideoUrl: randomUrl()
	}

	user_data.workerIndustryIds.push(randomIndustry());

	return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveUser', { body: user_data, json: true });	
}


function makeUserWithLikedAndPassedJobs(){
	let userId;
	let jobId;

	makeUser()
	.then(user_res => {
		userId = user_res.body.user_key;
		return makeJob();
	})
	.then(job_res => {
		jobId = job_res.body.job_key;
		return makeUserLikedJobs(userId, jobId);
	})
	.then(user_job_res => {
		return makeUserPassedJobs(userId, jobId);
	})
	.then(user_passed_res => {

	})
	.catch(err => {
		console.log('err', err);
	});
}


function makeUserTeam(){
	let user_team_data = {};

	makeUser()
	.then(user_res => {
		user_team_data.userId = user_res.body.user_key;
		return makeTeam();
	})
	.then(team_res => {
		user_team_data.teamId = [team_res.body.team_key];
		return got.post('https://us-central1-example-b8f70.cloudfunctions.net/saveUserTeams', { body: user_team_data, json: true })
	})
	.then(user_team_res => {

	})
	.catch(err => {
		console.log('Error: ', err);
	})

}


function populateData(obj, num){
	switch(obj) {

		case "team" :  
			for (let i = 0; i < num; i++) {
				console.log(`Creating team ${i+1}...`);
				makeATeam();
			}
			break;
		
		case "user" :
			for (let i = 0; i < num; i++) {
				console.log(`Creating user ${i+1}...`);
				makeUserWithLikedAndPassedJobs();
			}
			break;

		case "user-team" :
			for (let i = 0; i < num; i++) {
				console.log(`Creating user team ${i+1}...`);
				makeUserTeam();
			}
			break;

		default :
			console.log('choose an option....');
			break;
	
	}
}

populateData(program.object, program.count);