# Firebase Cloud Pilot

## Automation Script

Example :

* Creating 5 teams:

`node script.js --object=team --count=5`

* Creating 10 users:

`node script.js --object=user --count=10`

* Creating 2 user teams:

`node script.js --object=user-team --count=2`

> By default _object_ is **team** and _count_ is **10**

## API for filtering teams

* https://us-central1-example-b8f70.cloudfunctions.net/newFilter

| Name           | Type     | Description |
| :------------- | :------- | :---------- |
| industryIds    | `Array`  | Mandatory   |
| userId         | `String` | Mandatory   |
| count          | `Number` | Optional    |
| startingTeamId | `String` | Optional    |
