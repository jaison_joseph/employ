const images = {
  camera_icon: require('./Camera.png'),
  back_icon: require('./Chevron.png'),
}

export default images
