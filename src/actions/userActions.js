import * as Types from '../constants/actionTypes'
export const onUserResponse = user => ({ type: Types.ON_USER_RESPONSE, user })
