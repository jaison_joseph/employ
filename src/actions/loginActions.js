import * as Types from '../constants/actionTypes'
import * as firebase from 'firebase'

export const login = () => {
  return function(dispatch) {
    try {
      firebase
        .auth()
        .signInAnonymously()
        .then(function() {
          firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
              // User is signed in.
              var isAnonymous = user.isAnonymous
              let uid = user.uid
              dispatch(loginSuccess(uid))
            }
          })
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code
          var errorMessage = error.message
        })
    } catch (error) {
      console.log('error', error)
    }
  }
}
export const loginSuccess = userId => ({ type: Types.LOGIN_SUCCESS, userId })
