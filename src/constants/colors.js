const colors = {
  background: '#ffffff',
  black: '#3e3d45',
  grey: '#aaaab3',
}

export default colors
