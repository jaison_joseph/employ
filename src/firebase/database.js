/**
 * @class Database
 */

import * as firebase from 'firebase'
import { log } from '@firebase/database/dist/esm/src/core/util/util'

class Database {
  /**
   * Sets a users mobile number
   * @param userId
   * @param mobile
   * @returns {firebase.Promise<any>|!firebase.Promise.<void>}
   */
  static setUser(userId, userObject) {
    let userPath = '/user/'

    return firebase
      .database()
      .ref(userPath)
      .set({
        [userId]: userObject,
      })
  }

  /**
   * Listen for changes to a users mobile number
   * @param userId
   * @param callback Users mobile number
   */
  static listenUser(userId, callback) {
    let userPath = '/user/'
    let user
    firebase
      .database()
      .ref(userPath)
      .on('value', snapshot => {
        var userId = ''

        if (snapshot.val()) {
          user = snapshot.val()
        }

        callback(user)
      })
  }
}

module.exports = Database
