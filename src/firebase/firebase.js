import * as firebase from 'firebase'

class Firebase {
  /**
   * Initialises Firebase
   */
  static initialise() {
    firebase.initializeApp({
      apiKey: 'AIzaSyBagLQTrnJKJCVr-B3MJGLqNb6AVkHIKfU',
      authDomain: 'employ-ileaf.firebaseapp.com',
      databaseURL: 'https://employ-ileaf.firebaseio.com',
      storageBucket: 'employ-ileaf.appspot.com',
    })
  }
}

module.exports = Firebase
