/* Login Reducer
 * handles login states in the app
 */
import createReducer from '@lib/createReducer'
import * as actionTypes from '@constants/actionTypes'

const state = {
  ileaf: {
    user: {
      userId: -1,
      firstName: '',
      lastName: '',
      photoUrl: '',
    },
  },
}

export const userReducer = createReducer(state, {
  [actionTypes.LOGIN_SUCCESS](state, action) {
    return {
      ...state,
      ileaf: {
        user: {
          ...state.ileaf.user,
          userId: action.userId,
        },
      },
    }
  },
  [actionTypes.ON_USER_RESPONSE](state, action) {
    return {
      ...state,
      ileaf: {
        user: {
          ...state.ileaf.user,
          firstName: action.user.firstName,
          lastName: action.user.lastName,
          photoUrl: action.user.photoUrl,
        },
      },
    }
  },
})
