/* class combines all th existing reducers in the app
 */

import nav from './nav'
import * as userReducer from './userReducer'

export default Object.assign({ nav }, userReducer)
