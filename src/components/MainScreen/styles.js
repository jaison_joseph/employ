import { StyleSheet } from 'react-native'
import metrics from '../../constants/metrics'
import colors from '../../constants/colors'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.background,
  },
  profileImageView: {
    height: metrics.images.xlarge + 20,
    width: metrics.images.xlarge + 20,
  },
  profileImage: {
    height: metrics.images.xlarge,
    width: metrics.images.xlarge,
    borderRadius: metrics.images.xlarge / 2,
    marginTop: metrics.doubleSection,
  },
  camera_icon_view: {
    position: 'absolute',
    width: metrics.icons.large,
    height: metrics.icons.large,
    borderRadius: metrics.icons.large / 2,
    right: 0,
    bottom: 0,
    borderWidth: StyleSheet.hairlineWidth * 8,
    borderColor: colors.background,
    backgroundColor: colors.grey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  camera_icon: {
    width: metrics.icons.medium,
    height: metrics.icons.medium,
    borderRadius: metrics.icons.medium / 2,
    resizeMode: 'contain',
  },
  formData: {
    padding: 50,
    alignItems: 'center',
  },
  detailsView: {
    width: metrics.screenWidth,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textPrompt: {
    color: colors.grey,
    fontSize: 16,
  },
  textInput: {
    width: metrics.screenWidth / 2,
    borderBottomWidth: StyleSheet.hairlineWidth * 2,
    borderBottomColor: 'grey',
    marginLeft: 5,
    color: colors.black,
    fontSize: 16,
  },
  submit: {
    paddingTop: 30,
  },
})

export default styles
