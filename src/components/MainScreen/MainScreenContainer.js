import React, { Component } from 'react'
import { connect } from 'react-redux'
import MainScreen from './MainScreen'
import { onUserResponse } from '../../actions/userActions'

class MainScreenContainer extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return <MainScreen {...this.props} />
  }
}

function mapStateToProps(state) {
  return {
    user: state.userReducer,
  }
}
function mapDispatchToProps(dispatch) {
  return {
    onUserResponse: user => dispatch(onUserResponse(user)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(MainScreenContainer)
