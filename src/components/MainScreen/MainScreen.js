import React, { Component } from 'react'
import {
  View,
  TextInput,
  Image,
  Text,
  TouchableOpacity,
  BackHandler,
  Platform,
} from 'react-native'
import styles from './styles'
import FirebaseDB from '../../firebase/database'
import Firebase from '../../firebase/firebase'
import * as firebase from 'firebase'
import NavBar from '../general/NavBar/NavBar'
import images from '@images'
import RNFetchBlob from 'react-native-fetch-blob'
var ImagePicker = require('react-native-image-picker')
var options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
}
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class DatabaseConnect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      photoUrl: '',
      saveText: 'Save',
      avatarSource: '',
      avatarUri: '',
      path: '',
      imageUpload: false,
    }
    this.callBack = this.callBack.bind(this)
    this.editDetails = this.editDetails.bind(this)
  }

  componentDidMount() {
    console.log('this.props.user', this.props.user)
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
    FirebaseDB.listenUser(this.props.user.ileaf.user.userId, this.callBack)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }

  handleBackButton = () => {
    BackHandler.exitApp()
    return true
  }

  callBack(user) {
    if (user) {
      let userId = this.props.user.ileaf.user.userId
      let userDet = user[userId]

      this.setState({
        firstName: userDet.firstName,
        lastName: userDet.lastName,
        photoUrl: userDet.photoUrl,
      })
    } else {
      this.setState({
        firstName: this.props.user.ileaf.user.firstName,
        lastName: this.props.user.ileaf.user.lastName,
        photoUrl: this.props.user.ileaf.user.photoUrl,
      })
    }
  }

  render() {
    console.log('saved state', this.state.saveText)
    return (
      <View style={styles.container}>
        <NavBar
          title="Profile"
          back_icon={images.back_icon}
          rightButton={this.state.saveText}
          rightOnPress={this.editDetails}
          goBack={() => this.goBack}
        />

        {this.renderProfileImage()}
        <View style={styles.formData}>
          {this.renderDetails('First name', 'firstName')}
          {this.renderDetails('Last name', 'lastName')}
        </View>
      </View>
    )
  }

  goBack = () => {
    BackHandler.exitApp()
  }

  renderProfileImage() {
    return (
      <View style={styles.profileImageView}>
        <Image
          source={{ uri: this.state.photoUrl }}
          style={styles.profileImage}
        />
        <TouchableOpacity
          style={styles.camera_icon_view}
          onPress={() => {
            this.setState({ saveText: 'Save' })
            this.pickImage()
          }}
        >
          <Image source={images.camera_icon} style={styles.camera_icon} />
        </TouchableOpacity>
      </View>
    )
  }

  pickImage = () => {
    ImagePicker.launchImageLibrary(options, response => {
      console.log('Response = ', response)

      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        let source = { uri: response.uri }
        console.log('response path', response.path)
        this.setState({
          avatarSource: source,
          avatarUri: response.uri,
          path: response.path,
        })
        this.uploadImage(this.state.avatarUri)
          .then(url => {
            this.setState({ photoUrl: url, imageUpload: true })
          })
          .catch(error => {
            console.log(error)
            this.setState({ imageUpload: false })
          })
      }
    })
  }

  uploadImage = (uri, mime = 'image/jpg') => {
    return new Promise((resolve, reject) => {
      const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
      const sessionId = new Date().getTime()
      let uploadBlob = null
      const imageRef = firebase
        .storage()
        .ref('images')
        .child(`${sessionId}`)

      fs
        .readFile(uploadUri, 'base64')
        .then(data => {
          return Blob.build(data, { type: `${mime};BASE64` })
        })
        .then(blob => {
          uploadBlob = blob
          return imageRef.put(blob, { contentType: mime })
        })
        .then(() => {
          uploadBlob.close()
          return imageRef.getDownloadURL()
        })
        .then(url => {
          console.log('response url', url)
          resolve(url)
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  renderDetails(text, stateValue) {
    return (
      <View style={styles.detailsView}>
        <Text style={styles.textPrompt}>{text}</Text>
        <TextInput
          onChangeText={name =>
            this.setState({ [stateValue]: name, saveText: 'Save' })
          }
          value={this.state[stateValue]}
          style={styles.textInput}
          underlineColorAndroid={'transparent'}
        />
      </View>
    )
  }

  editDetails = () => {
    if (!this.state.imageUpload) {
      this.userDetails(this.props.user.ileaf.user.photoUrl)
    } else {
      this.userDetails(this.state.photoUrl)
    }
  }

  userDetails(url) {
    let user = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      photoUrl: url,
    }
    this.props.onUserResponse(user)
    FirebaseDB.setUser(this.props.user.ileaf.user.userId, user)

    this.setState({ saveText: 'Saved!' })
  }
}

export default DatabaseConnect
