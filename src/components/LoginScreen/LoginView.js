/**
 * @class Login
 */

import { Text, View, Button } from 'react-native'

import React, { Component } from 'react'
import * as firebase from 'firebase'
import Firebase from '../../firebase/firebase'
import styles from './styles'

class Login extends Component {
  constructor(props) {
    super(props)
    if (!firebase.apps.length) {
      Firebase.initialise()
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.submit}>
          <Button
            onPress={() => this.props.login()}
            title="Login Anonymously"
          />
        </View>
      </View>
    )
  }
}

module.exports = Login
