import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginView from './LoginView'
import { login } from '../../actions/loginActions'

let navigated = false

class LoginContainer extends Component {
  constructor(props) {
    super(props)
    navigated = false
  }

  componentWillReceiveProps(props) {
    if (props.user.ileaf.user.userId.length > 0 && !navigated) {
      props.navigation.navigate('Main')
      navigated = true
    }
  }

  componentDidMount() {
    if (this.props.user.ileaf.user.userId.length > 0 && !navigated) {
      navigated = true
      this.props.navigation.navigate('Main')
    }
  }

  render() {
    return <LoginView {...this.props} />
  }
}

function mapStateToProps(state) {
  return {
    user: state.userReducer,
  }
}
function mapDispatchToProps(dispatch) {
  return {
    login: () => dispatch(login()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
