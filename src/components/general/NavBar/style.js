import { StyleSheet, Platform } from 'react-native'
import colors from '@constants/colors'
import metrics from '@constants/metrics'
const NAVBAR_HEIGHT = Platform.OS == 'ios' ? 64 : 54
const NAVBAR_PADDINGTOP = Platform.OS == 'ios' ? 10 : 0

const styles = StyleSheet.create({
  navbar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: NAVBAR_HEIGHT,
    width: metrics.screenWidth,
    paddingTop: NAVBAR_PADDINGTOP,
    backgroundColor: colors.background,
    paddingLeft: 5,
    paddingRight: 5,
  },
  leftButton: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  title: {
    color: colors.black,
    fontWeight: 'bold',
    fontSize: 16,
  },
  savedStyle: {
    color: 'green',
  },
  rightButton: {},
})

export default styles
