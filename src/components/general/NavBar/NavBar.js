import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

import style from './style'

const NavBar = props => {
  console.log('props: ', props.rightButton)
  return (
    <View style={style.navbar}>
      <TouchableOpacity style={style.leftButton} onPress={props.goBack()}>
        <Image source={props.back_icon} style={{ height: 30, width: 30 }} />
      </TouchableOpacity>
      <Text style={style.title}>{props.title}</Text>
      <TouchableOpacity style={style.rightButton} onPress={props.rightOnPress}>
        <Text
          style={props.rightButton === 'Save' ? style.title : style.savedStyle}
        >
          {props.rightButton}
        </Text>
      </TouchableOpacity>
    </View>
  )
}
export default NavBar
