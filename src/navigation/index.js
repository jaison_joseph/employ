import { StackNavigator } from 'react-navigation'

import LoginScreen from '@components/LoginScreen'
import MainScreen from '@components/MainScreen'

const EmployApp = StackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: { header: null, getsturesEnabled: false },
  },
  Main: {
    screen: MainScreen,
    navigationOptions: { header: null, getsturesEnabled: false },
  },
})

export default EmployApp
