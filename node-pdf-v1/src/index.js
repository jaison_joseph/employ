/** 
 * Setup required libraries
 * 1. fs
 * 2. ejs
 * 3. pdf
 * 4. config
 * 5. nodemailer
 * 6. moment
 */
import fs from 'fs';
import ejs from 'ejs';
import pdf from 'html-pdf';
import nodemailer from 'nodemailer';
import moment from 'moment';

let config = {
    "imageUrl": "file:///ileaf/projects/node-pdf/",
    "pdfPath": "/home/ileaf/projects/node-pdf/",
    "mailConfig": {
        "fromName": "qatestileaf",
        "fromMail": "qatestileaf@gmail.com",
        "smtpUsername": "qatestileaf@gmail.com",
        "smtpPassword": "forileaf2018",
        "toAddress": "rino@ileafsolutions.net"
    }
}


/**
 * If startOfWeek and endOfWeek not specified then get the current week's start and end dates 
 * TODO: Inorder to test with current weeks data in json, use the commented code or you can manually input a startdate and enddate
 */
let startOfWeek: string = moment('2017-01-01T01:33:05.183Z').startOf('week').toDate();  // Start date of week input- moment().startOf('week').toDate() 
let endOfWeek: string = moment('2018-03-19T01:33:05.183Z').endOf('week').toDate(); // End date of week input- moment().endOf('week').toDate()
let teamId: string = "-L4iEC-swKjUeOZzMWxE";// TeamId input.



/**
 * Options used in PDF Generation 
 */
let options = { format: 'Legal', quality: 75, orientation: "portrait" };
/**
 * Nodemailer transporter
 */
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mailConfig.smtpUsername,
        pass: config.mailConfig.smtpPassword
    }
})


/**
 * Read JSON data in db-export.json file 
 * Author  : iLeaf Solutions 
 */

function readJsonData() {
    return new Promise((resolve, reject) => {
        fs.readFile('db-export.json', 'utf8', (err, jsonData) => {
            if (err)
                reject(err)
            resolve(jsonData)
        })
    })
}


/**
 * Read JSON data in db-export.json file 
 * Author  : iLeaf Solutions 
 */

async function getJsonData(jsonData: mixed) {
    
    let jsonOutput: mixed = {};
    let timeIndex: number;
    let userIndex: number;

    /**
    * Add current week startDate and endDate 
    */
    jsonOutput = {
        "startDate": moment(startOfWeek).format('D MMMM'),
        "endDate": moment(endOfWeek).format('D MMMM'),
        "days": [],
        "companyLogo": config.imageUrl + 'logo.png',
        "weekData": config.imageUrl + 'week-text.png',
        "emptyImageUrl": config.imageUrl + 'empty-avatar.png'
    };


    let jsonObject: object = JSON.parse(jsonData);
    let teamShifts: object = jsonObject["team-shifts"];


    /**
     * Get teamShifts from jsonObject uing teamId
     */
    let shifts = teamShifts[teamId];


    /**
     * Looping shifts
     */
    for (let shiftId in shifts) {
        if (shifts.hasOwnProperty(shiftId)) {

            /**
             * Consider only data have startDate and endDate.
             */
            if (shifts[shiftId].startDate && shifts[shiftId].endDate) {

                let startDate: string = moment(shifts[shiftId].startDate);
                let endDate: string = moment(shifts[shiftId].endDate);

                /**
                 * if startOfWeek and endOfWeek week data only 
                 */
                if (moment(startOfWeek).diff(startDate) <= 0 && moment(endOfWeek).diff(endDate) >= 0) {

                    /**
                     * Get shift-ShiftTimings form jsonObject uing shiftId
                     */
                    let shiftShiftTimings: mixed = jsonObject["shift-shiftTimings"][shiftId];

                    if (shiftShiftTimings) {
                        jsonOutput = shiftTiming(shiftShiftTimings, shiftId, startDate, jsonObject, jsonOutput)

                    } else {
                        console.log("shiftShiftTimings Not Found in input json object and shiftId is :", shiftId)
                    }
                } else {
                    console.log("Skip data, Not this Week data", shiftId)
                }

            } else { //skipped shiftId
                console.log("skip shiftId, Empty startDate or endDate", shiftId)
            }
        }
    }//End shifts for loop

    jsonOutput = sortJsonObject(jsonOutput)
    return jsonOutput
}

/**
 * Render html file 
 * Author : iLeaf Solutions 
 * @param {Object} htmlInput 
 */

function renderEjs(htmlInput) {
    return new Promise((resolve, reject) => {
        ejs.renderFile('pdf-template.ejs', htmlInput, (err, renderHtml) => {
            if (err)
                reject(err);
            resolve(renderHtml)
        })
    })
}


/**
 * PDF Generation 
 * Author : iLeaf Solutions 
 * @param {String} renderHtml 
 */

function generatePdf(renderHtml) {
    return new Promise((resolve, reject) => {
        pdf.create(renderHtml, options).toFile('./wallNewTeam.pdf', (err, res) => {
            if (err) {
                reject(err);
            } else {
                console.log(res); // { filename: '/app/businesscard.pdf' }
                resolve()
            }
        })
    })
}


/**
 * Send mail  
 * Author : iLeaf Solutions 
 */
function sendMail() {
    return new Promise((resolve, reject) => {
        let mailOptions = {
            from: '"' + config.mailConfig.fromName + '" <' + config.mailConfig.fromMail + '>',// sender address
            to: config.mailConfig.toAddress, // list of receivers 
            subject: "Shift Details",
            html: "Hello,<br><br>Please find the attached pdf.<br>",
            attachments: [
                {   // file on disk as an attachment
                    filename: 'wall.pdf',
                    path: config.pdfPath + 'wallNewTeam.pdf' // stream this file
                }
            ]
        };

        // Send the Mail
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log("Error from mail sending: ", error);
            } else {
                console.log("Mail sent successfully")
            }
        });
    })
}


/**
 * Base function to fetch data, process, genrate pdf and to send mail 
 * Author : iLeaf Solutions 
 */

async function createPdf() {

    try {
        let jsonData = await readJsonData()
        let getData = await getJsonData(jsonData)

        let htmlInput : mixed = {
            data: getData
        };

        let htmlData = await renderEjs(htmlInput)
        await generatePdf(htmlData)
        // await sendMail()

    } catch (err) {
        console.log("something went wrong", err)
    }
}

createPdf(); //Main function call

/**
 * find shiftTime Id , get shiftTiming data return jsonOutput
 * Author : iLeaf Solutions
 */

 function shiftTiming(shiftShiftTimings, shiftId, startDate,jsonObject, jsonOutput) {

    for (let shiftTimingId in shiftShiftTimings) {
        if (shiftShiftTimings.hasOwnProperty(shiftTimingId)) {
            let shiftTiming: object = jsonObject["shiftTiming"][shiftTimingId];

            if (shiftTiming) {
                /**
                 * Get shift-shiftAssignments form jsonObject using shiftTimingId
                 */
                let shiftShiftAssignments: object = jsonObject["shift-shiftAssignments"][shiftId];

                if (shiftShiftAssignments) {
                    jsonOutput = loopShifAssignments(shiftShiftAssignments, startDate, shiftId, shiftTiming, jsonObject, jsonOutput)
                } else {
                    console.log("shiftShiftAssignments Not Found in input json object and shiftId is :", shiftId)
                }
            } else {
                console.log("shiftTiming Not Found in input json object and shiftTimingId is :", shiftTimingId)
            }
        }
    }// End Loop shiftShiftTimings
    return jsonOutput
}

/**
* find shiftAssignment Id ,get shiftTiming data and return jsonOutput
* Author : iLeaf Solutions
*/

function loopShifAssignments(shiftShiftAssignments, startDate, shiftId, shiftTiming, jsonObject, jsonOutput ) {

    for (let shiftAssignmentId in shiftShiftAssignments) {
        if (shiftShiftAssignments.hasOwnProperty(shiftAssignmentId)) {
            let shiftAssignment: object = jsonObject["shiftAssignment"][shiftAssignmentId];
            let userId: string = shiftAssignment.userId;
            if (userId) {

                /**
                 * Get user form jsonObject using userId
                 */
                let user = jsonObject["user"][userId];

                if (user) {

                    /**
                     * Get shiftAssignment-shiftSlots form jsonObject using shiftAssignmentId
                     */
                    let shiftAssignmentShiftSlots = jsonObject["shiftAssignment-shiftSlots"][shiftAssignmentId];

                    /**
                     * If any shiftAssignmentId available in shiftAssignment-shiftSlots, exclude that user from the list.
                     */
                    if (shiftAssignmentShiftSlots) {//exclude that user from the list
                        console.log("Skip user, user Id exist shiftAssignmentShiftSlots")
                    } else {

                        let assignmentShiftId = shiftAssignment.shiftId;

                        if (assignmentShiftId) {
                            /**
                            * Get shift form jsonObject using ShiftId
                            */
                            let shift = jsonObject["shift"][assignmentShiftId];

                            if (shift) {
                                let roleId = shift.roleId;

                                if (roleId) {
                                    /**
                                     * Get role form jsonObject using roleId
                                     */
                                    let role = jsonObject["role"][roleId];

                                    if (role) {
                                        let roleName = role.name;
                                        jsonOutput = createJsonObject(startDate, roleName, shiftTiming, userId, user, jsonOutput)

                                    }
                                }
                            }
                        } else {
                            console.log("Assignment Shift Id Not Found in input json object, shiftId: ", shiftId)
                        }
                    }
                } else {
                    console.log("user Not Found in input json object and userId is :", userId);
                }
            } else {
                console.log("User Id Not Found in input json object, shiftId: ", shiftId)
            }// End if User Id
        }
    }//End Loop shiftShiftAssignments
    
    return jsonOutput
}

/**
* Create Out Put JSON Object
* Author : iLeaf Solutions
*/

function createJsonObject(startDate, roleName, shiftTiming, userId, user, jsonOutput) {
    
    let dateIndex: number = jsonOutput.days.findIndex(findDateIndex);
    if (dateIndex < 0) { //date not exist 
        jsonOutput.days.unshift({
            startDay: moment(startDate).format('dddd'),
            date: moment(startDate).format('D MMMM'),
            shifts: []
        });
        dateIndex = 0;
    }
    /**
    * Find Index of date
    * @param {Array} day 
    */
    function findDateIndex(day) {
        return day.date == moment(startDate).format('D MMMM');
    }


    /**
    * Update or upload shifts in jsonOutput
    */
    let shiftIndex: number = jsonOutput.days[dateIndex].shifts.findIndex(findShiftIndex);

    if (shiftIndex < 0) { //shift not exist 
        jsonOutput.days[dateIndex].shifts.unshift({
            position: roleName,
            timeArray: []
        });
        shiftIndex = 0;
    }
    /**
    * Find Index of shift
    * @param {Array} shift 
    */
    function findShiftIndex(shift) {
        return shift.position == roleName;
    }


    /**
    * Update or upload times in jsonOutput
    */
    let timeIndex: number = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.findIndex(findTimeIndex);

    if (timeIndex < 0) { //time not exist 
        jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.unshift({
            startTime: moment(shiftTiming.startTime).format('HH.mm a'),
            endTime: moment(shiftTiming.endTime).format('HH.mm a'),
            users: []
        });
        timeIndex = 0;
    }
    /**
    * Find Index of time
    * @param {Array} time 
    */
    function findTimeIndex(time) {
        return (time.startTime == moment(shiftTiming.startTime).format('HH.mm a') && time.endTime == moment(shiftTiming.endTime).format('HH.mm a'));
    }
    
    /**
    * Update or upload user in jsonOutput
    */
    let userIndex: number = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.findIndex(findUserIndex);

    if (userIndex < 0) { //user not exist 
        jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.unshift({
            id: userId,
            name: user.firstName,
            imageUrl: user.photoUrl ? user.photoUrl : ""
        });
        userIndex = 0;
    }
    /**
    * Find Index of user
    * @param {Array} user 
    */
    function findUserIndex(user) {
        return user.id == user.userId;
    }

    return jsonOutput
}

/**
* Sort jsonOutPut according to date
* Author : iLeaf Solutions
*/

function sortJsonObject(jsonOutput) {

    jsonOutput.days.sort(function (day1, day2) {
        return moment.utc(new Date(day1.date)).diff(moment.utc(new Date(day2.date)))
    });

    for (let day in jsonOutput.days) {

        jsonOutput.days[day].shifts.sort(compareByName);

        for (let shift in jsonOutput.days[day].shifts) {
            /**
             * Sort start Time
             */
            jsonOutput.days[day].shifts[shift].timeArray.sort(function (time1, time2) {
                return moment.utc(time1.startTime).diff(moment.utc(time2.startTime))
            });

            for (let time in jsonOutput.days[day].shifts[shift].timeArray) {

                /**
                 * Sort User Name
                 */
                for (let user in jsonOutput.days[day].shifts[shift].timeArray[time]) {


                    jsonOutput.days[day].shifts[shift].timeArray[time].users.sort(compareByName);
                }
            }
        }
    }
    return jsonOutput


    /**
     * Name Sorting 
     * @param {*} user1 
     * @param {*} user2 
     */
    function compareByName(user1, user2) {
        if (user1.name < user2.name)
            return -1;
        if (user1.name > user2.name)
            return 1;
        return 0;
    }
}








