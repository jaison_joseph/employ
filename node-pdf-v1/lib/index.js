'use strict';

/**
 * Read JSON data in db-export.json file 
 * Author  : iLeaf Solutions 
 */

let getJsonData = (() => {
    var _ref = _asyncToGenerator(function* (jsonData) {

        let jsonOutput = {};
        let timeIndex;
        let userIndex;

        /**
        * Add current week startDate and endDate 
        */
        jsonOutput = {
            "startDate": (0, _moment2.default)(startOfWeek).format('D MMMM'),
            "endDate": (0, _moment2.default)(endOfWeek).format('D MMMM'),
            "days": [],
            "companyLogo": config.imageUrl + 'logo.png',
            "weekData": config.imageUrl + 'week-text.png',
            "emptyImageUrl": config.imageUrl + 'empty-avatar.png'
        };

        let jsonObject = JSON.parse(jsonData);
        let teamShifts = jsonObject["team-shifts"];

        /**
         * Get teamShifts from jsonObject uing teamId
         */
        let shifts = teamShifts[teamId];

        /**
         * Looping shifts
         */
        for (let shiftId in shifts) {
            if (shifts.hasOwnProperty(shiftId)) {

                /**
                 * Consider only data have startDate and endDate.
                 */
                if (shifts[shiftId].startDate && shifts[shiftId].endDate) {

                    let startDate = (0, _moment2.default)(shifts[shiftId].startDate);
                    let endDate = (0, _moment2.default)(shifts[shiftId].endDate);

                    /**
                     * if startOfWeek and endOfWeek week data only 
                     */
                    if ((0, _moment2.default)(startOfWeek).diff(startDate) <= 0 && (0, _moment2.default)(endOfWeek).diff(endDate) >= 0) {

                        /**
                         * Get shift-ShiftTimings form jsonObject uing shiftId
                         */
                        let shiftShiftTimings = jsonObject["shift-shiftTimings"][shiftId];

                        if (shiftShiftTimings) {
                            jsonOutput = shiftTiming(shiftShiftTimings, shiftId, startDate, jsonObject, jsonOutput);
                        } else {
                            console.log("shiftShiftTimings Not Found in input json object and shiftId is :", shiftId);
                        }
                    } else {
                        console.log("Skip data, Not this Week data", shiftId);
                    }
                } else {
                    //skipped shiftId
                    console.log("skip shiftId, Empty startDate or endDate", shiftId);
                }
            }
        } //End shifts for loop

        jsonOutput = sortJsonObject(jsonOutput);
        return jsonOutput;
    });

    return function getJsonData(_x) {
        return _ref.apply(this, arguments);
    };
})();

/**
 * Render html file 
 * Author : iLeaf Solutions 
 * @param {Object} htmlInput 
 */

/**
 * Base function to fetch data, process, genrate pdf and to send mail 
 * Author : iLeaf Solutions 
 */

let createPdf = (() => {
    var _ref2 = _asyncToGenerator(function* () {

        try {
            let jsonData = yield readJsonData();
            let getData = yield getJsonData(jsonData);

            let htmlInput = {
                data: getData
            };

            let htmlData = yield renderEjs(htmlInput);
            yield generatePdf(htmlData);
            // await sendMail()
        } catch (err) {
            console.log("something went wrong", err);
        }
    });

    return function createPdf() {
        return _ref2.apply(this, arguments);
    };
})();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _ejs = require('ejs');

var _ejs2 = _interopRequireDefault(_ejs);

var _htmlPdf = require('html-pdf');

var _htmlPdf2 = _interopRequireDefault(_htmlPdf);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /** 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Setup required libraries
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 1. fs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 2. ejs
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 3. pdf
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 4. config
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 5. nodemailer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * 6. moment
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */


let config = {
    "imageUrl": "file:///ileaf/projects/node-pdf/",
    "pdfPath": "/home/ileaf/projects/node-pdf/",
    "mailConfig": {
        "fromName": "qatestileaf",
        "fromMail": "qatestileaf@gmail.com",
        "smtpUsername": "qatestileaf@gmail.com",
        "smtpPassword": "forileaf2018",
        "toAddress": "rino@ileafsolutions.net"
    }

    /**
     * If startOfWeek and endOfWeek not specified then get the current week's start and end dates 
     * TODO: Inorder to test with current weeks data in json, use the commented code or you can manually input a startdate and enddate
     */
};let startOfWeek = (0, _moment2.default)('2017-01-01T01:33:05.183Z').startOf('week').toDate(); // Start date of week input- moment().startOf('week').toDate() 
let endOfWeek = (0, _moment2.default)('2018-03-19T01:33:05.183Z').endOf('week').toDate(); // End date of week input- moment().endOf('week').toDate()
let teamId = "-L4iEC-swKjUeOZzMWxE"; // TeamId input.


/**
 * Options used in PDF Generation 
 */
let options = { format: 'Legal', quality: 75, orientation: "portrait" };
/**
 * Nodemailer transporter
 */
let transporter = _nodemailer2.default.createTransport({
    service: 'gmail',
    auth: {
        user: config.mailConfig.smtpUsername,
        pass: config.mailConfig.smtpPassword
    }
});

/**
 * Read JSON data in db-export.json file 
 * Author  : iLeaf Solutions 
 */

function readJsonData() {
    return new Promise((resolve, reject) => {
        _fs2.default.readFile('db-export.json', 'utf8', (err, jsonData) => {
            if (err) reject(err);
            resolve(jsonData);
        });
    });
}function renderEjs(htmlInput) {
    return new Promise((resolve, reject) => {
        _ejs2.default.renderFile('pdf-template.ejs', htmlInput, (err, renderHtml) => {
            if (err) reject(err);
            resolve(renderHtml);
        });
    });
}

/**
 * PDF Generation 
 * Author : iLeaf Solutions 
 * @param {String} renderHtml 
 */

function generatePdf(renderHtml) {
    return new Promise((resolve, reject) => {
        _htmlPdf2.default.create(renderHtml, options).toFile('./wallNewTeam.pdf', (err, res) => {
            if (err) {
                reject(err);
            } else {
                console.log(res); // { filename: '/app/businesscard.pdf' }
                resolve();
            }
        });
    });
}

/**
 * Send mail  
 * Author : iLeaf Solutions 
 */
function sendMail() {
    return new Promise((resolve, reject) => {
        let mailOptions = {
            from: '"' + config.mailConfig.fromName + '" <' + config.mailConfig.fromMail + '>', // sender address
            to: config.mailConfig.toAddress, // list of receivers 
            subject: "Shift Details",
            html: "Hello,<br><br>Please find the attached pdf.<br>",
            attachments: [{ // file on disk as an attachment
                filename: 'wall.pdf',
                path: config.pdfPath + 'wallNewTeam.pdf' // stream this file
            }]
        };

        // Send the Mail
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log("Error from mail sending: ", error);
            } else {
                console.log("Mail sent successfully");
            }
        });
    });
}

createPdf(); //Main function call

/**
 * find shiftTime Id , get shiftTiming data return jsonOutput
 * Author : iLeaf Solutions
 */

function shiftTiming(shiftShiftTimings, shiftId, startDate, jsonObject, jsonOutput) {

    for (let shiftTimingId in shiftShiftTimings) {
        if (shiftShiftTimings.hasOwnProperty(shiftTimingId)) {
            let shiftTiming = jsonObject["shiftTiming"][shiftTimingId];

            if (shiftTiming) {
                /**
                 * Get shift-shiftAssignments form jsonObject using shiftTimingId
                 */
                let shiftShiftAssignments = jsonObject["shift-shiftAssignments"][shiftId];

                if (shiftShiftAssignments) {
                    jsonOutput = loopShifAssignments(shiftShiftAssignments, startDate, shiftId, shiftTiming, jsonObject, jsonOutput);
                } else {
                    console.log("shiftShiftAssignments Not Found in input json object and shiftId is :", shiftId);
                }
            } else {
                console.log("shiftTiming Not Found in input json object and shiftTimingId is :", shiftTimingId);
            }
        }
    } // End Loop shiftShiftTimings
    return jsonOutput;
}

/**
* find shiftAssignment Id ,get shiftTiming data and return jsonOutput
* Author : iLeaf Solutions
*/

function loopShifAssignments(shiftShiftAssignments, startDate, shiftId, shiftTiming, jsonObject, jsonOutput) {

    for (let shiftAssignmentId in shiftShiftAssignments) {
        if (shiftShiftAssignments.hasOwnProperty(shiftAssignmentId)) {
            let shiftAssignment = jsonObject["shiftAssignment"][shiftAssignmentId];
            let userId = shiftAssignment.userId;
            if (userId) {

                /**
                 * Get user form jsonObject using userId
                 */
                let user = jsonObject["user"][userId];

                if (user) {

                    /**
                     * Get shiftAssignment-shiftSlots form jsonObject using shiftAssignmentId
                     */
                    let shiftAssignmentShiftSlots = jsonObject["shiftAssignment-shiftSlots"][shiftAssignmentId];

                    /**
                     * If any shiftAssignmentId available in shiftAssignment-shiftSlots, exclude that user from the list.
                     */
                    if (shiftAssignmentShiftSlots) {
                        //exclude that user from the list
                        console.log("Skip user, user Id exist shiftAssignmentShiftSlots");
                    } else {

                        let assignmentShiftId = shiftAssignment.shiftId;

                        if (assignmentShiftId) {
                            /**
                            * Get shift form jsonObject using ShiftId
                            */
                            let shift = jsonObject["shift"][assignmentShiftId];

                            if (shift) {
                                let roleId = shift.roleId;

                                if (roleId) {
                                    /**
                                     * Get role form jsonObject using roleId
                                     */
                                    let role = jsonObject["role"][roleId];

                                    if (role) {
                                        let roleName = role.name;
                                        jsonOutput = createJsonObject(startDate, roleName, shiftTiming, userId, user, jsonOutput);
                                    }
                                }
                            }
                        } else {
                            console.log("Assignment Shift Id Not Found in input json object, shiftId: ", shiftId);
                        }
                    }
                } else {
                    console.log("user Not Found in input json object and userId is :", userId);
                }
            } else {
                console.log("User Id Not Found in input json object, shiftId: ", shiftId);
            } // End if User Id
        }
    } //End Loop shiftShiftAssignments

    return jsonOutput;
}

/**
* Create Out Put JSON Object
* Author : iLeaf Solutions
*/

function createJsonObject(startDate, roleName, shiftTiming, userId, user, jsonOutput) {

    let dateIndex = jsonOutput.days.findIndex(findDateIndex);
    if (dateIndex < 0) {
        //date not exist 
        jsonOutput.days.unshift({
            startDay: (0, _moment2.default)(startDate).format('dddd'),
            date: (0, _moment2.default)(startDate).format('D MMMM'),
            shifts: []
        });
        dateIndex = 0;
    }
    /**
    * Find Index of date
    * @param {Array} day 
    */
    function findDateIndex(day) {
        return day.date == (0, _moment2.default)(startDate).format('D MMMM');
    }

    /**
    * Update or upload shifts in jsonOutput
    */
    let shiftIndex = jsonOutput.days[dateIndex].shifts.findIndex(findShiftIndex);

    if (shiftIndex < 0) {
        //shift not exist 
        jsonOutput.days[dateIndex].shifts.unshift({
            position: roleName,
            timeArray: []
        });
        shiftIndex = 0;
    }
    /**
    * Find Index of shift
    * @param {Array} shift 
    */
    function findShiftIndex(shift) {
        return shift.position == roleName;
    }

    /**
    * Update or upload times in jsonOutput
    */
    let timeIndex = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.findIndex(findTimeIndex);

    if (timeIndex < 0) {
        //time not exist 
        jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray.unshift({
            startTime: (0, _moment2.default)(shiftTiming.startTime).format('HH.mm a'),
            endTime: (0, _moment2.default)(shiftTiming.endTime).format('HH.mm a'),
            users: []
        });
        timeIndex = 0;
    }
    /**
    * Find Index of time
    * @param {Array} time 
    */
    function findTimeIndex(time) {
        return time.startTime == (0, _moment2.default)(shiftTiming.startTime).format('HH.mm a') && time.endTime == (0, _moment2.default)(shiftTiming.endTime).format('HH.mm a');
    }

    /**
    * Update or upload user in jsonOutput
    */
    let userIndex = jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.findIndex(findUserIndex);

    if (userIndex < 0) {
        //user not exist 
        jsonOutput.days[dateIndex].shifts[shiftIndex].timeArray[timeIndex].users.unshift({
            id: userId,
            name: user.firstName,
            imageUrl: user.photoUrl ? user.photoUrl : ""
        });
        userIndex = 0;
    }
    /**
    * Find Index of user
    * @param {Array} user 
    */
    function findUserIndex(user) {
        return user.id == user.userId;
    }

    return jsonOutput;
}

/**
* Sort jsonOutPut according to date
* Author : iLeaf Solutions
*/

function sortJsonObject(jsonOutput) {

    jsonOutput.days.sort(function (day1, day2) {
        return _moment2.default.utc(new Date(day1.date)).diff(_moment2.default.utc(new Date(day2.date)));
    });

    for (let day in jsonOutput.days) {

        jsonOutput.days[day].shifts.sort(compareByName);

        for (let shift in jsonOutput.days[day].shifts) {
            /**
             * Sort start Time
             */
            jsonOutput.days[day].shifts[shift].timeArray.sort(function (time1, time2) {
                return _moment2.default.utc(time1.startTime).diff(_moment2.default.utc(time2.startTime));
            });

            for (let time in jsonOutput.days[day].shifts[shift].timeArray) {

                /**
                 * Sort User Name
                 */
                for (let user in jsonOutput.days[day].shifts[shift].timeArray[time]) {

                    jsonOutput.days[day].shifts[shift].timeArray[time].users.sort(compareByName);
                }
            }
        }
    }
    return jsonOutput;

    /**
     * Name Sorting 
     * @param {*} user1 
     * @param {*} user2 
     */
    function compareByName(user1, user2) {
        if (user1.name < user2.name) return -1;
        if (user1.name > user2.name) return 1;
        return 0;
    }
}